import _ from 'lodash';
import Vue from "vue";
import dateinput from "./dateinput.vue";
import datagrid from "./datagrid.vue";


var app = new Vue({
    el: '#app',
    components: {
        dateinput,
        datagrid
    },
    methods:{

    }
}); 